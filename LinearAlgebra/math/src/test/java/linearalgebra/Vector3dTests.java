package linearalgebra;

//Uyen Dinh Michelle Banh
//2234181

import static org.junit.Assert.*;
import org.junit.Test;

public class Vector3dTests {
    @Test
    public void return_good_results(){
        Vector3d v3d = new Vector3d(1, 2, 3);
        assertEquals(1, v3d.getX(), 0.0001);
        assertEquals(2, v3d.getY(), 0.0001);
        assertEquals(3, v3d.getZ(), 0.0001);
    }

    @Test
    public void confirm_magnitude(){
        Vector3d v3d = new Vector3d(1, 2, 3);
        assertEquals(Math.sqrt(14), v3d.magnitude(), 0.001);
    }

    @Test
    public void dot_product_return_good(){
        Vector3d v3d = new Vector3d(1, 2, 3);
        Vector3d v3d2 = new Vector3d(4, 5, 6);

        assertEquals(32, v3d.dotProduct(v3d2), 0.001);
    }

    @Test
    public void addTest_good(){
        Vector3d v3d = new Vector3d(1, 2, 3);
        Vector3d v3d2 = new Vector3d(4, 5, 6); 

        Vector3d v3d_good = v3d.add(v3d2);

        assertEquals(5, v3d_good.getX(), 0.00001);
        assertEquals(7, v3d_good.getY(), 0.00001);
        assertEquals(9, v3d_good.getZ(), 0.00001);
    }
}
